# BioSim T20 Moen Oian
Exam project inf200 June 2021

## Developers
Bastian Undheim Øian and Torjus Strandenes Moen

## Modelling the Ecosystem of Rossumøya
The project is modelling the Ecosystem of Rossumøya on behalf of the Environmental Protection Agency of Pylandia (EPAP).
Rossumøya is a small island in the middle of the ocean that belongs to the nation of Pylandia.
We have created this simulation to study the stability of Rossumøya's ecosystem.
The long term goal is to preserve Rossumøya as a nature park for future generations.

## Visualization
A visualisation is provided in one graphics window, and consists of seven matplotlib subplots.
The visualisation will include:

* A year counter showing the current simulated year
* A map of the simulated island, with legend
* A Line graph showing the total number of animals per species
* A population map showing the number of animals per cell
* Three histograms showing the distribution of *age*, *weight* and *fitness*

## Usage
For further description of the project, including usage examples and class descriptions, see documentation.
### Documentation
To view documentation, run *make html* in the *docs* folder.