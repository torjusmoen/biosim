Animals
========

The island consists of two species of animal: Herbivore and Carnivore.

The animals share most characteristics, but differ in how they eat.


Common characteristics
#######################
Aging
******
The aging method is run each year and updates the animals accordingly:

* The animal's attribute *age* increases by one
* The animal's attribute *weight* decreases by a factor η
* The animal's attribute *fitness* is updated
* The animal's attribute *has_migrated* is reset to the default value False

Fitness (Φ)
************
The animal's *fitness* is calculated by its weight attribute and the parameters φ\ :sub:`age`,  a\ :sub:`1/2`, φ\ :sub:`weight` and ω\ :sub:`1/2`

* If the *weight* is zero, the *fitness* will be zero
* If the *weight* is not zero, the *fitness* is calculated with the following function:
    * :math:`Φ = q^+(a, a_{1/2}, φ_{age} × q^-(ω, ω_{1/2}, ω_{weight})`
    * :math:`q^±(x, x_{1/2}, φ) = \frac{1}{(1 + e^{±φ(x−x_{1/2})})}`

.. note::
    The *fitness* can only be of values 0 ≤ Φ ≤ 1.

Weight (ω)
***********
The animal's *weight* is calculated by drawing a random sample from a gaussian distribution.
:math:`ω \sim N(ω_{birth}, σ_{birth})`

Birth
******
If there are two or more animals on a cell, the animals can give birth to a new animal of the same species.
Gender plays no role in mating for this simulation.
Each animal can only give birth to one other animal each year.
The probability of a birth is calculated with the following function:

* :math:`min(1,γ×Φ×(N−1))`
* where γ is a constant, Φ is the animal's *fitness* and *N* is the number of animals in the cell
* This means that the probability is zero if there is only one animal of the species in the cell
* The probability of birth is also zero if the *weight* is :math:`ω < ζ(ω_{birth} + σ_{birth}).`

When an animal does give birth it loses *weight* by a factor of ξ.
If the animal would lose more than its own *weight*, it does not give birth.

Death
******
At the end of each year the animals have a chance to die.
If the animal's *weight* is zero, it dies, otherwise the probability of death is calculated with the following function:

* :math:`ω(1−Φ)`

Migration
**********
Each year the animals can migrate once to a neighboring cell.
The chance to migrate is dependent on the animal's *fitness* with the following function:

* :math:`μΦ`

The animal's destination cell is chosen at random between the four potential cells.
Animals cannot migrate to a Water cell.


Specific characteristics
#########################
The animals differ in how the consume fodder.

Herbivores
***********
Herbivores consume fodder found on the cells.
See :doc:`/cell` for a description of fodder in the cells.
Animals will eat in a random order.
Each animal tries to eat an amount, F, of fodder, but how much fodder the animal obtains depends
on the fodder available in the cell.
The animals weight *increases* by a factor β multiplied by the amount of fodder eaten.

Carnivores
***********
Carnivores consume herbivores.
The carnivores attempt killing herbivores in order of *fitness*, that is to say,
the carnivore with the highest *fitness* eats first and tries first to kill the herbivore with the lowest *fitness*.
The carnivore continues to attempt killing until:

* The carnivore has eaten an amount F of fodder
* Or attempted killing every herbivore in the cell

The probability of killing is given by the following function:

* :math:`p = 0`, if :math:`Φ_{carn} <= Φ_{herb}`
* :math:`p = \frac{Φ_{carn} - Φ_{herb}}{∆Φ_{max}}`, if :math:`0 < Φ_{carn} - Φ_{herb} < ∆Φ_{max}`
* :math:`p = 1` otherwise


Class: Animals
###############
.. autoclass:: biosim.animals.Animals
    :members:

Class: Herbivore
#################
.. autoclass:: biosim.animals.Herbivore
    :members:

Class: Carnivore
#################
.. autoclass:: biosim.animals.Carnivore
    :members:

.. warning::
    A ValueError will be raised if the animals are given invalid parameters or attributes.

    e.g. negative age or :math:`μ > 0`
