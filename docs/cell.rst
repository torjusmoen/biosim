Cell
=====
The island of Rossumøya is divided into cells.
There are four types of cells on the island:
Highland_,
Lowland_,
Desert_ and
Water_.

.. note::
    The amount of fodder :math:`f_{max}` can be set with the set_parameters method in island.py


Highland
########
The highland cell is accessible to the animals.

The cell contains a low amount of fodder for the herbivores, by default :math:`f_{max} = 300.0`.

The amount of fodder resets to :math:`f_{max}` every year.

See :doc:`/island` for a more thorough description of the islands yearly cycle.


Lowland
########
The lowland cell is accessible to the animals.

The cell contains a high amount of fodder for the herbivores, by default :math:`f_{max} = 800.0`.

The amount of fodder resets to :math:`f_{max}` every year.

See :doc:`/island` for a more thorough description of the islands yearly cycle.


Desert
#######
The desert cell is accessible to the animals.

The cell contains no fodder for the herbivores, by default :math:`f_{max} = 0.0`.


Water
######
The water cell is **not** accessible to the animals.


Class: Cell
############
.. autoclass:: biosim.cell.Cell
    :members:

Class: Highland
################
.. autoclass:: biosim.cell.Highland
    :members:

Class: Lowland
################
.. autoclass:: biosim.cell.Lowland
    :members:

Class: Desert
##############
.. autoclass:: biosim.cell.Desert
    :members:

Class: Water
#############
.. autoclass:: biosim.cell.Water
    :members:

.. warning::
    A ValueError will be raised if the cells are given invalid parameters
    or animals are placed where they have no access.

    e.g. :math:`f_{max} > 0` or Herbivore placed on a Water_ cell
