Documentation for the BioSim project
=====================================
Developers
-----------
Torjus Strandenes Moen and Bastian Undheim Øian

|

Project
--------
BioSim is a population simulation software.

The project is modelling the Ecosystem of Rossumøya on behalf of the Environmental Protection Agency of Pylandia (EPAP).
Rossumøya is a small island in the middle of the ocean that belongs to the nation of Pylandia.
We have created this simulation to study the stability of Rossumøya's ecosystem.
The long term goal is to preserve Rossumøya as a nature park for future generations.

Since EPAP encountered some political controversy about the reliability of the simulations,
EPAP placed great emphasis on the quality of our project.
All code development is traceable through regular commits to a version control system,
including all exchange of code between team members.

|

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

|

Modules
========
.. toctree::
   :maxdepth: 2

   animals
   cell
   island
   simulation

|

Tests
======
Tests for the modules can be found in the *test* folder and
contains:

* test_animal.py
* test_cells.py
* test_island.py
* test_biosim.py

The project has been created with test-driven development using the PyTest framework
and automated test runners in GitLab.

The tests include basic unit tests, statistical tests and tests with fixtures.

|

.. license added in accordance with https://www.gnu.org/licenses/gpl-howto.html

License
========
    Copyright (C) 2021  Torjus Strandenes Moen, Bastian Undheim Øian

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

.. figure:: gplv3-or-later.png
   :alt: gplv3-or-later-license
   :align: center
