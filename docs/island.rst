Island
=======

Geography
##########
Rossumøya's geography is defined by a Python multi-line string specification.
This string is always of equal line length and each letter consists only of a valid :doc:`\cell`.
The island is always surrounded by Water cells, this means that no animals can migrate off the island.

The island is a dictionary with the keys cell location and values cell class instances which contain the animals.

.. figure:: map.png
    :width: 200
    :alt: Example map of Rossumøya

Figure 1: Example map of Rossumøya. Blue: Water, dark green: lowland, light green: highland, yellow: desert.


Annual cycle
#############
Each year Rossumøya goes through a set annual cycle. The cycle is as follows:

Feeding
--------
The animals take turns eating:

* The herbivores eat first in a random order
* Then the carnivores eat in a fixed order by fitness

Procreation
------------
The animals give birth:

* The chance of a birth taking place is described in :doc:`\animals`

Migration
----------
The animals migrate to adjacent cells:

* The chance of migration taking place is described in :doc:`\animals`

Aging
------
The animals and island age:

* The animals age like described in :doc:`\animals`

* The fodder on the cells is reset to the default amount :math:`f_{max}`

Death
------
The animals die:

* The chance of death is described in :doc:`\animals`


Class: Island
##############
.. autoclass:: biosim.island.Island
    :members:


