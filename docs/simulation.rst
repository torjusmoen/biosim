Simulation
===========
Usage
######
To run a simulation initialize the BioSim class found in *simulation.py*.

Example simulation running for 10 years with 20 herbivores on a small island:

.. code-block:: python
    :emphasize-lines: 14, 15

    geogr = """\
            WWWWW
            WLHLW
            WHHHW
            WDDDW
            WWWWW"""

    ini_pop = [{'loc': (3, 3),
                  'pop': [{'species': 'Herbivore',
                  'age': 5,
                  'weight': 20}
                  for _ in range(20)]}]

    simulation = BioSim(geogr, ini_pop, seed=2222)
    simulation.simulate(10)

To save a log file, images and a video animation, specify *log_file*,
*img_dir*, *img_base* and call the *make_movie* method:

.. code-block:: python
    :emphasize-lines: 2, 3, 4, 6

    simulation = BioSim(geogr, ini_herbs + ini_carns, seed=1234,
                 log_file='results/log.txt',
                 img_dir='results',
                 img_base='sample')
    simulation.simulate(10)
    simulation.make_movie()

.. note::

    The BioSim class must be given the following parameters:

    :parameter island_map: Multi-line string specifying island geography

    |

    :parameter ini_pop: List of dictionaries specifying initial population

    |

    :parameter seed: Integer used as random number seed

.. warning::
    The make_movie method can only make mp4 or gif.
    Specify your format with the *movie_fmt* parameter (mp4 by default).

Visualization
##############
A visualisation is provided in one graphics window, and consists of seven matplotlib subplots.
The visualisation will include:

    * A year counter showing the current simulated year
    * A map of the simulated island, with legend
    * A Line graph showing the total number of animals per species
    * A population map showing the number of animals per cell
    * Three histograms showing the distribution of *age*, *weight* and *fitness*

.. figure:: vis.png
    :alt: Example visualisation

Figure 2: Example visualization.

.. warning::
    No visualization will be shown if *vis_years* is set to 0


Class: BioSim
##############
.. autoclass:: biosim.simulation.BioSim
    :members: