#!python
# -*- coding: utf-8 -*-
__author__ = "Bastian Undheim Øian, Torjus Strandenes Moen"
__email__ = "bastian.undheim.oian@nmbu.no, torjus.strandenes.moen@nmbu.no"

import textwrap
from biosim.simulation import BioSim

if __name__ == '__main__':
    geogr = """\
               WWWWWWWWW
               WLLLLLLLW
               WLLLLLLLW
               WLLLLLLLW
               WLLLLLLLW
               WLLLLLLLW
               WLLLLLLLW
               WLLLLLLLW
               WWWWWWWWW"""
    geogr = textwrap.dedent(geogr)

    """
    Animal parameters gotten from  INF200 Lecture No Ju06.
    The parameters ensure no birth, no death and no killing.
    """
    ini_herbs = [{'loc': (5, 5),
                  'pop': [{'species': 'Herbivore',
                           'age': 5,
                           'weight': 50}
                          for _ in range(1000)]}]
    ini_carns = [{'loc': (5, 5),
                  'pop': [{'species': 'Carnivore',
                           'age': 5,
                           'weight': 50}
                          for _ in range(1000)]}]

    sim = BioSim(island_map=geogr, ini_pop=ini_herbs + ini_carns,
                 seed=14556,
                 vis_years=1,
                 img_base="test",
                 img_dir=None,
                 img_fmt="png"
                 )
    sim.set_animal_parameters('Herbivore',
                              {'mu': 2, 'omega': 0, 'gamma': 0,
                               'a_half': 1000})
    sim.set_animal_parameters('Carnivore',
                              {'mu': 2, 'omega': 0, 'gamma': 0,
                               'F': 0, 'a_half': 1000})
    sim.simulate(num_years=25)
