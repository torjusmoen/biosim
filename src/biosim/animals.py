"""
Copyright (C) 2021 Torjus Strandenes Moen, Bastian Undheim Øian

This file is part of BioSim.

    BioSim is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BioSim is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BioSim.  If not, see <https://www.gnu.org/licenses/>.
"""
# !python
# -*- coding: utf-8 -*-

__author__ = "Bastian Undheim Øian, Torjus Strandenes Moen"
__email__ = "bastian.undheim.oian@nmbu.no, torjus.strandenes.moen@nmbu.no"

import math
import random


class Animals:
    """
    A parent class for the animals
    """
    parameters = {}

    def __init__(self, age=0, weight=None):
        """
        The animal class constructor.
        Initializes animal with age and weight.

        Parameters
        ----------
        age: int
            The animal's age
        weight: float
            The animal's weight
        """
        # age must be a positive number
        if age < 0:
            raise ValueError("The age must be a positive integer")
        else:
            self.age = age

        # calculates the weight if none is given, must be a positive number
        if weight is None:
            self.weight = self.calculate_weight()
        elif weight <= 0:
            raise ValueError("The weight must be greater than zero")
        else:
            self.weight = weight

        # calculates the animal's fitness
        self.fitness = self.calculate_fitness()

        # sets has_migrates as False by default
        self.has_migrated = False

    @classmethod
    def set_parameters(cls, params):
        """
        Sets animal parameters.

        Parameters
        ----------
        params: dict
            Dictionary of new parameters
        """
        for param in params.items():
            # other parameter values shall be positive
            if param[1] < 0:
                raise ValueError(f"{param[0]} cannot have a negative value")
            # special case for eta
            elif param[0] == "eta" and param[1] > 1:
                raise ValueError("eta must be positive and smaller or equal to one")
                # special case for DeltaPhiMax
            elif param[0] == "DeltaPhiMax" and param[1] <= 0:
                raise ValueError("DeltaPhiMax must be greater than zero")
            else:
                cls.parameters[param[0]] = param[1]

    def aging(self):
        """
        Makes the animal age one year,
        updates the weight, fitness and resets has_migrated to the default False.
        """
        # increases age by one for each year
        self.age += 1

        # decreases the weight by eta * self.weight
        self.weight -= (self.weight * self.parameters["eta"])

        # updates the fitness with the new age and weight
        self.fitness = self.calculate_fitness()

        # sets migration status as not migrated this year
        self.has_migrated = False

    def calculate_death(self):
        """
        Calculates if an animal will die.

        Returns
        -------
        bool
            True if the animal will die
        """
        # death is certain if weight is zero
        if self.weight == 0:
            return True

        # calculates probability of death and converts it to bool
        else:
            return random.random() < self.parameters["omega"] * (1 - self.fitness)

    def calculate_fitness(self):
        """
        Calculates the fitness with the given function.
        If the animal's weight is zero, the animals fitness is zero.

        Returns
        -------
        fitness: float
            the animal's fitness

        """
        # given formula
        q_plus = 1 / (1 + math.exp(
            self.parameters["phi_age"] * (self.age - self.parameters["a_half"]))
                      )
        q_minus = 1 / (1 + math.exp(
            -self.parameters["phi_weight"] * (self.weight - self.parameters["w_half"]))
                      )

        # fitness is zero if the weight is less or equal to zero
        if self.weight <= 0:
            fitness = 0
        # calculates fitness from given equation
        else:
            fitness = q_plus * q_minus
        return fitness

    def calculate_weight(self):
        """
        Calculates the weight of the animal using a gaussian distribution.

        Returns
        -------
        weight: float
            the animal's weight
        """
        # pulls a random sample from a gaussian distribution
        # with mean w_birth and standard deviation sigma_birth
        weight = random.gauss(
            self.parameters["w_birth"], self.parameters["sigma_birth"]
        )
        return weight

    def calculate_birth(self, N):
        """
        Calculates if a birth will take place.

        Parameters
        ----------
        N: int
            The number of animals in the cell

        Returns
        -------
        bool
            True if the animal will have a birth
        """
        # the probability of birth is zero if the weight is less than zeta * (w_birth + sigma_birth)
        if self.weight < (self.parameters["zeta"] *
                          (self.parameters["w_birth"] + self.parameters["sigma_birth"])):
            return False

        # calculates the probability of birth and converts it to bool
        else:
            return random.random() < min(1, self.parameters["gamma"] * self.fitness * (N - 1))

    def give_birth(self, N):
        """
        Gives birth to a new animal if "calculate_birth" is True.
        The animal will have default parameter values.
        Updates the mother's weight.

        Parameters
        ----------
        N: int
            The number of animals in the cell

        Returns
        -------
        birth_weight: float
            The weight of the newborn animal
        """
        birth_weight = self.calculate_weight()

        # checks if calculate_birth is True and if the animal weighs enough to give birth
        if self.calculate_birth(N) \
                and self.weight > (self.parameters["xi"] * birth_weight):

            # updates the animal's weight after giving birth
            self.weight -= (self.parameters["xi"] * birth_weight)

            # updates the mother's fitness
            self.fitness = self.calculate_fitness()

            return birth_weight

    def calculate_migration(self):
        """
        Calculates if the animal will migrate.

        Returns
        -------
        bool
            True if the animal will migrate
        """
        return random.random() < self.parameters["mu"] * self.fitness


class Herbivore(Animals):
    """
    A subclass for the herbivore species
    """
    # given parameters to be used in functions
    parameters = {
        "w_birth": 8.0,
        "sigma_birth": 1.5,
        "beta": 0.9,
        "eta": 0.05,
        "a_half": 40.0,
        "phi_age": 0.6,
        "w_half": 10.0,
        "phi_weight": 0.1,
        "mu": 0.25,
        "gamma": 0.2,
        "zeta": 3.5,
        "xi": 1.2,
        "omega": 0.4,
        "F": 10.0
    }

    def __init__(self, age=0, weight=None):
        """
        The herbivore class constructor.
        Initializes herbivore with age and weight.

        Parameters
        ----------
        age: int
            The animal's age
        weight: float
            The animal's weight
        """
        super().__init__(age, weight)

    def consume_fodder(self, available_fodder):
        """
        Increases the animal's weight after it has consumed fodder.

        Parameters
        ----------
        available_fodder: float
            The amount of fodder available to the animal

        Returns
        -------
        eaten: float
            The amount of fodder eaten by the animal
        """
        eaten = self.parameters["F"]
        if available_fodder < eaten:
            eaten = available_fodder
        # increased weight by beta * fodder
        self.weight += eaten * self.parameters["beta"]
        # updates the fitness
        self.fitness = self.calculate_fitness()
        return eaten


class Carnivore(Animals):
    """
    A subclass for the herbivore species
    """
    # given parameters to be used in functions
    parameters = {
        "w_birth": 6.0,
        "sigma_birth": 1.0,
        "beta": 0.75,
        "eta": 0.125,
        "a_half": 40.0,
        "phi_age": 0.3,
        "w_half": 4.0,
        "phi_weight": 0.4,
        "mu": 0.4,
        "gamma": 0.8,
        "zeta": 3.5,
        "xi": 1.1,
        "omega": 0.8,
        "F": 50.0,
        "DeltaPhiMax": 10.0
    }

    def __init__(self, age=0, weight=None):
        """
        The carnivore class constructor.
        Initializes carnivore with age and weight.

        Parameters
        ----------
        age: int
            The animal's age
        weight: float
            The animal's weight
        """
        super().__init__(age, weight)

    def check_fitness_ratio(self, prey):
        """
        Checks if the carnivore's fitness is between zero and the prey's fitness.

        Parameters
        ----------
        prey: Herbivore class object
            A herbivore
        Returns
        -------
        bool
            True if the carnivore's fitness is between zero and the prey's fitness
        """
        return 0 < self.fitness - prey.fitness <= self.parameters["DeltaPhiMax"]

    def calculate_kill_prob(self, prey):
        """
        Calculates the probability of a carnivore killing a herbivore.

        Parameters
        ----------
        prey: Herbivore class object

        Returns
        -------
        float
            The probability of the carnivore killing the prey
        """
        return (self.fitness - prey.fitness) / self.parameters["DeltaPhiMax"]

    def eat_herbivores(self, herbs_in_cell):
        """
        The carnivore tries to eat the herbivores in the same cell.

        Parameters
        ----------
        herbs_in_cell: list
            List of herbivores in the cell

        Returns
        -------
        eaten_herbs: list
            List of herbivores the carnivore ate
        """
        eaten_herbs = []
        num_herbs = len(herbs_in_cell)
        attempts = 1
        weight_eaten = 0

        for herb in herbs_in_cell:
            if weight_eaten < self.parameters["F"] and attempts <= num_herbs:
                # if the carnivore's fitness is lower than the herbivore, no kill happens
                if self.fitness <= herb.fitness:
                    prob = 0
                # if the carnivore's fitness is between the herbivore and DeltaPhiMax,
                # uses given equation for probability
                elif self.check_fitness_ratio(herb):
                    prob = self.calculate_kill_prob(herb)
                # if the carnivore's fitness is greater than DeltaPhiMax, a kill happens
                else:
                    prob = 1

                # if the carnivore eats, increase weight and update fitness
                if random.random() < prob:
                    self.weight += herb.weight * self.parameters["beta"]
                    self.calculate_fitness()
                    eaten_herbs.append(herb)
                    weight_eaten += herb.weight

                attempts += 1
        return eaten_herbs
