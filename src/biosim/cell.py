"""
Copyright (C) 2021 Torjus Strandenes Moen, Bastian Undheim Øian

This file is part of BioSim.

    BioSim is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BioSim is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BioSim.  If not, see <https://www.gnu.org/licenses/>.
"""
# !python
# -*- coding: utf-8 -*-

__author__ = "Bastian Undheim Øian, Torjus Strandenes Moen"
__email__ = "bastian.undheim.oian@nmbu.no, torjus.strandenes.moen@nmbu.no"

import random
import itertools
from biosim.animals import Herbivore, Carnivore


class Cell:
    """
    A parent class for all cells
    """
    # parameters used by cells
    parameters = {
        # by default cells have no fodder
        "f_max": 0.0
    }

    def __init__(self, animal_list):
        """
        The cell class constructor.
        Initializes cells with population.

        Parameters
        ----------
        animal_list: list
            List of animals to be placed in the cell
        """
        # adds the starting amount of starting fodder to the available fodder
        self.available_fodder = Cell.parameters["f_max"]

        self.adjacent_cells = []

        # empty lists for the animals separated by species
        self.pop_herbivore = []
        self.pop_carnivore = []

        # adds the animals to their respective population lists
        for animal in animal_list:
            if animal["species"] == "Herbivore":
                self.pop_herbivore.append(Herbivore(animal["age"], animal["weight"]))
            else:
                self.pop_carnivore.append(Carnivore(animal["age"], animal["weight"]))

    def amount_herbivore(self):
        """
        Checks how many herbivores are on the cell.

        Returns
        -------
        int
            Number of herbivores in the cell
        """
        return len(self.pop_herbivore)

    def amount_carnivore(self):
        """
        Checks how many carnivores are on the cell.

        Returns
        -------
        int
            Number of carnivores in the cell
        """
        return len(self.pop_carnivore)

    def add_animals(self, animals):
        """
        Adds a population of animals to the cell.

        Parameters
        ----------
        animals: list
            List of animals to be added to the cell
        """
        for animal in animals:
            if animal["species"] == "Herbivore":
                self.pop_herbivore.append(Herbivore(animal["age"], animal["weight"]))
            elif animal["species"] == "Carnivore":
                self.pop_carnivore.append(Carnivore(animal["age"], animal["weight"]))

    def add_baby(self):

        """
        Iterates over all animals and tries to give birth.
        If an animal gives birth the baby is added to the cell.
        """
        pop = self.amount_herbivore() + self.amount_carnivore()
        num_herb = self.amount_herbivore()
        num_carn = self.amount_carnivore()
        # checks if there are animals on the cell
        if pop != 0:
            # runs give_birth and adds the baby to the cell fro herbivores
            for herb in self.pop_herbivore[:num_herb]:
                birth_weight = herb.give_birth(num_herb)
                if birth_weight is not None:
                    self.pop_herbivore.append(Herbivore(age=0, weight=birth_weight))

            # runs give_birth and adds the baby to the cell fro herbivores
            for carn in self.pop_carnivore[:num_carn]:
                birth_weight = carn.give_birth(num_carn)
                if birth_weight is not None:
                    self.pop_carnivore.append(Carnivore(age=0, weight=birth_weight))

    def regenerate_fodder(self):
        """
        Regenerates the cells fodder.
        """
        self.available_fodder = self.parameters["f_max"]

    def eating(self):
        """
        The animals take turns eating.
        """
        # herbivores eat
        # randomly sorts the herbivores
        random.shuffle(self.pop_herbivore)
        for herb in self.pop_herbivore:
            eaten = herb.consume_fodder(self.available_fodder)
            self.available_fodder -= eaten

        # carnivores eat
        # sorts the herbivores and carnivores by fitness
        carn_sorted = self.sort_carnivores(carn for carn in self.pop_carnivore)
        herbs_in_cell = self.sort_herbivores(herb for herb in self.pop_herbivore)

        for carn in carn_sorted:
            eaten_herbs = carn.eat_herbivores(herbs_in_cell)
            # removes the eaten herbivores from the cells herbivore population
            herbs_in_cell = [herb for herb in herbs_in_cell if herb not in eaten_herbs]

        self.pop_herbivore = herbs_in_cell

    @staticmethod
    def sort_carnivores(carn):
        """
        Sorts the list of carnivores by fitness, sorted in ascending order.

        Parameters
        ----------
        carn: list
            List of carnivores to be sorted

        Returns
        -------
        list
            List of sorted carnivores
        """
        return sorted(carn, key=lambda x: x.fitness, reverse=True)

    @staticmethod
    def sort_herbivores(herb):
        """
        Sorts the list of herbivores by fitness, sorted in descending order.

        Parameters
        ----------
        herb: list
            List of herbivores to be sorted

        Returns
        -------
        list
            List of sorted herbivores
        """
        return sorted(herb, key=lambda x: x.fitness)

    @classmethod
    def set_parameters(cls, params):
        """
        Sets parameters for the cells.

        Parameters
        ----------
        params: dict
            Dictionary of the new parameters.
        """
        for param in params.items():
            # changes the parameter if it's a known parameter
            if param[0] in cls.parameters.keys():
                # special case for DeltaPhiMax
                if param[0] == "f_max" and param[1] < 0:
                    raise ValueError("Cell cannot have a negative amount of fodder")
                else:
                    cls.parameters[param[0]] = param[1]
            else:
                raise ValueError(f"Unknown parameter {param[0]}")

    def aging(self):
        """
        Ages each animal in the population.
        """
        for animal in itertools.chain(self.pop_herbivore, self.pop_carnivore):
            animal.aging()

    def death(self):
        """
        Keeps the animals that get False from calculate_death.
        """
        if len(self.pop_herbivore) != 0:
            self.pop_herbivore = \
                [herb for herb in self.pop_herbivore if not herb.calculate_death()]
        if len(self.pop_carnivore) != 0:
            self.pop_carnivore = \
                [carn for carn in self.pop_carnivore if not carn.calculate_death()]

    def migrate(self):
        """
        Migrates the animal if the animal has not moved, and calculate_migration gives True.
        The cell the animal moves to is randomly chosen between the four adjacent cells.
        If the chosen cell is accessible the animal will migrate there, if not the animal stays.
        """
        leaving_animals = {}
        for animal in itertools.chain(self.pop_herbivore, self.pop_carnivore):
            # if the animal has not yet moved and is going to
            if animal.has_migrated is False and animal.calculate_migration() is True:
                choice = random.choice(self.adjacent_cells)
                # if the cell is accessible
                if choice.access is True:
                    animal.has_migrated = True
                    leaving_animals[animal] = choice

        # Emigrates the leaving animals from the cell
        self.pop_herbivore = \
            [herb for herb in self.pop_herbivore if herb not in leaving_animals.keys()]
        self.pop_carnivore = \
            [carn for carn in self.pop_carnivore if carn not in leaving_animals.keys()]

        # Immigrates the leaving animals to their new cells
        for migration in leaving_animals.items():
            if type(migration[0]) is Herbivore:
                migration[1].pop_herbivore.append(migration[0])
            else:
                migration[1].pop_carnivore.append(migration[0])


class Highland(Cell):
    """
    A subclass for the highland cells.
    """
    # sets the cell's fodder amount
    parameters = {
        "f_max": 300.0
    }
    # the animal can access the cell
    access = True

    def __init__(self, animal_list):
        """
        Initializes the highland cell

        Parameters
        ----------
        animal_list: list
            List of animals to be added to population
        """
        super().__init__(animal_list)


class Lowland(Cell):
    """
    A subclass for the lowland cells
    """
    # sets the cell's fodder amount
    parameters = {
        "f_max": 800.0
    }
    # the animal can access the cell
    access = True

    def __init__(self, animal_list):
        """
        Initializes the lowland cell

        Parameters
        ----------
        animal_list: list
            List of animals to be added to population
        """
        super().__init__(animal_list)


class Desert(Cell):
    """
    A subclass for the desert cells
    """
    # the animal can access the cell
    access = True

    def __init__(self, animal_list):
        """
        Initializes the desert cell

        Parameters
        ----------
        animal_list: list
            List of animals to be added to population
        """
        super().__init__(animal_list)


class Water(Cell):
    """
    A subclass for the water cells
    """
    # the animal cannot access the cell
    access = False

    def __init__(self, animal_list):
        """
        Initializes the water cell

        Parameters
        ----------
        animal_list: list
            List of animals to be added to population
        """
        super().__init__(animal_list)
