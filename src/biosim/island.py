"""
Copyright (C) 2021 Torjus Strandenes Moen, Bastian Undheim Øian

This file is part of BioSim.

    BioSim is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BioSim is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BioSim.  If not, see <https://www.gnu.org/licenses/>.
"""
# !python
# -*- coding: utf-8 -*-

__author__ = "Bastian Undheim Øian, Torjus Strandenes Moen"
__email__ = "bastian.undheim.oian@nmbu.no, torjus.strandenes.moen@nmbu.no"

from biosim.cell import Lowland, Highland, Water, Desert


class Island:
    """
    A class defining the island.
    """
    # assigns letters to the cell types
    cell_types = {
        "W": Water,
        "L": Lowland,
        "H": Highland,
        "D": Desert
    }

    def __init__(self, geogr, init_pop):
        """
        The island class constructor.
        Initializes island with geography and population.

        Parameters
        ----------
        geogr: str
            String defining the island geography
        init_pop: list
            list of the population on the island
        """
        self.geogr = geogr
        self.pop = init_pop
        self.cell_dict = {}
        self.pop_dict = {}
        self.island_map = {}

    def check_map_length(self):
        """
        Checks if all lines in the string have the same length.
        """
        line_len = []
        for line in self.geogr.splitlines():
            line_len.append(line)
        if not all(len(line) == len(line_len[0]) for line in line_len):
            raise ValueError("The lines in the string are not of equal length")

    def check_boundary_cells(self):
        """
        Checks if all the boundary cells are water cells.
        """
        bound_cell = []
        all_lines = []
        for line in self.geogr.splitlines():
            all_lines.append(line)

        for line_number in range(len(self.geogr.splitlines())):
            # adds the first line to bound_cell
            if line_number == 0:
                for cell in all_lines[0]:
                    bound_cell.append(cell)

            # adds the first and last cell of the middle lines to bound_cell
            elif 0 < line_number < (len(all_lines) - 1):
                middle_line = all_lines[line_number]
                bound_cell.append(middle_line[0])
                bound_cell.append(middle_line[-1])

            # adds the last line to bound_cell
            else:
                for cell in all_lines[-1]:
                    bound_cell.append(cell)

        # if bound cell isn't all W, the map does not have all water cells at its boundary
        if not all(elem == "W" for elem in bound_cell):
            raise ValueError("The boundary cells are not all water cells")

    def check_valid_map(self):
        """
        Checks if all the characters are valid cells
        """
        geogr = self.geogr.splitlines()
        valid = ["W", "H", "L", "D"]
        for line in geogr:
            for cell in line:
                if cell not in valid:
                    raise ValueError("The geography contains invalid cells")

    def check_map_conditions(self):
        """
        Checks if all lines in the string have the same length.
        Checks if the map only has water cells on its border.
        Checks if all characters used in the string are valid.
        """
        self.check_map_length()
        self.check_boundary_cells()
        self.check_valid_map()

    def create_island_map(self):
        """
        Creates the map by merging the cell and animal dictionaries.

        Key: coordinates
        Value: cell type and animal population
        """
        # creates the cell and population dictionaries
        self.create_cell_dict()
        self.create_pop_dict()

        for loc, cell_type in self.cell_dict.items():
            if cell_type == "W":
                if loc in self.pop_dict.keys():
                    raise ValueError("Animals can not be placed on the Water cell")
                else:
                    self.island_map[loc] = Water([])
            elif cell_type == "L":
                if loc in self.pop_dict.keys():
                    self.island_map[loc] = Lowland(self.pop_dict[loc])
                else:
                    self.island_map[loc] = Lowland([])
            elif cell_type == "H":
                if loc in self.pop_dict.keys():
                    self.island_map[loc] = Highland(self.pop_dict[loc])
                else:
                    self.island_map[loc] = Highland([])
            elif cell_type == "D":
                if loc in self.pop_dict.keys():
                    self.island_map[loc] = Desert(self.pop_dict[loc])
                else:
                    self.island_map[loc] = Desert([])
        # finds the cell's adjacent cells
        for loc, cell in self.island_map.items():
            self.find_adjacent_cells(loc, cell)

    def create_cell_dict(self):
        """
        Creates a dictionary of the island cells.

        Key: coordinates
        Value: cell type
        """
        # checks if map conditions are met
        self.check_map_conditions()

        # begins on coordinates (1,1)
        x = 1

        for line in self.geogr.splitlines():
            y = 1
            for cell in line:
                self.cell_dict[(x, y)] = cell
                y += 1
            x += 1

    def create_pop_dict(self):
        """
        Creates a dictionary of the init population.

        Key: coordinates
        Value: init population
        """
        for animal in self.pop:
            # if there already is an animal at a location the new animal extends the dictionary
            if animal["loc"] in self.pop_dict.keys():
                self.pop_dict[animal["loc"]].extend(animal["pop"])
            # if there are no animals at a location a new entry is created
            else:
                self.pop_dict[animal["loc"]] = animal["pop"]

    def add_population(self, population):
        """
        Adds a population to the existing island.

        Parameters
        ----------
        population: list
            list of population to be added
        """
        for animals in population:
            # raises ValueError if the animal is placed on a cell it can't access
            if not self.island_map[animals["loc"]].access:
                raise ValueError("Attempted animal placement on invalid cell")
            else:
                self.island_map[animals["loc"]].add_animals(animals["pop"])

    def annual_cycle(self):
        """
        Goes through the islands annual cycle.
        """
        self.regen_fodder()
        self.eating()
        self.migration()
        self.add_baby()
        self.aging()
        self.death()

    def regen_fodder(self):
        """
        Regenerates fodder on the island's cells.
        """
        for cell in self.island_map.values():
            cell.regenerate_fodder()
        # calls on cell.py function regenerate_fodder for each cell

    def eating(self):
        """
        Makes the island's animals eat.
        """
        for cell in self.island_map.values():
            cell.eating()
        # calls on cell.py function eating for each cell

    def find_adjacent_cells(self, cell_loc, current_cell):
        """
        Finds the adjacent cells of a given cell.

        Parameters
        ----------
        cell_loc: list
            List of the cells coordinates
        current_cell: Cell class object
            The given cell
        """
        # sets x and y as the coordinates of the current cell
        x, y = cell_loc
        # list of the coordinates of the adjacent cells
        potential_cells = [(x, y + 1), (x, y - 1), (x - 1, y), (x + 1, y)]
        for cell in potential_cells:
            if cell in self.island_map.keys():
                current_cell.adjacent_cells.append(self.island_map[cell])

    def migration(self):
        """
        Makes the island's animals migrate.
        """
        for cell in self.island_map.values():
            cell.migrate()

    def add_baby(self):
        """
        Makes the island's animals give birth.
        """
        for cell in self.island_map.values():
            cell.add_baby()

    def aging(self):
        """
        Makes the island's animals age.
        """
        for cell in self.island_map.values():
            cell.aging()

    def death(self):
        """
        Makes the island's animals die.
        """
        for cell in self.island_map.values():
            cell.death()
