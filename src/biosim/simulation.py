"""
Copyright (C) 2021 Torjus Strandenes Moen, Bastian Undheim Øian

This file is part of BioSim.

    BioSim is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BioSim is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BioSim.  If not, see <https://www.gnu.org/licenses/>.


Some code gotten from Hans Ekkehard Plesser / NMBU randvis project, licensed under the MIT License.

MIT License

Copyright (c) 2021 Hans Ekkehard Plesser / NMBU

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
# !python
# -*- coding: utf-8 -*-

__author__ = "Bastian Undheim Øian, Torjus Strandenes Moen"
__email__ = "bastian.undheim.oian@nmbu.no, torjus.strandenes.moen@nmbu.no"

from biosim.island import Island
from biosim.animals import Herbivore, Carnivore
from biosim.cell import Water, Highland, Lowland, Desert
import matplotlib.pyplot as plt
import random
import numpy as np
import subprocess
import os
_FFMPEG_BINARY = 'ffmpeg'
_MAGICK_BINARY = 'magick'


class BioSim:
    def __init__(self, island_map, ini_pop, seed,
                 vis_years=1, ymax_animals=None, cmax_animals=None, hist_specs=None,
                 img_dir=None, img_base=None, img_fmt='png', img_years=None,
                 log_file=None):

        """
        :param island_map: Multi-line string specifying island geography
        :param ini_pop: List of dictionaries specifying initial population
        :param seed: Integer used as random number seed
        :param ymax_animals: Number specifying y-axis limit for graph showing animal numbers
        :param cmax_animals: Dict specifying color-code limits for animal densities
        :param hist_specs: Specifications for histograms, see below
        :param vis_years: years between visualization updates (if 0, disable graphics)
        :param img_dir: String with path to directory for figures
        :param img_base: String with beginning of file name for figures
        :param img_fmt: String with file type for figures, e.g. 'png'
        :param img_years: years between visualizations saved to files (default: vis_years)
        :param log_file: If given, write animal counts to this file

        If ymax_animals is None, the y-axis limit should be adjusted automatically.
        If cmax_animals is None, sensible, fixed default values should be used.
        cmax_animals is a dict mapping species names to numbers, e.g.,
        {'Herbivore': 50, 'Carnivore': 20}

        hist_specs is a dictionary with one entry per property for which a histogram shall be shown.
        For each property, a dictionary providing the maximum value and the bin width must be
        given, e.g.,
        {'weight': {'max': 80, 'delta': 2}, 'fitness': {'max': 1.0, 'delta': 0.05}}
        Permitted properties are 'weight', 'age', 'fitness'.

        If img_dir is None, no figures are written to file. Filenames are formed as

        f'{os.path.join(img_dir, img_base}_{img_number:05d}.{img_fmt}'

        where img_number are consecutive image numbers starting from 0.

        img_dir and img_base must either be both None or both strings.
        """
        random.seed(seed)
        self.log_file = log_file
        self.sim_island = Island(island_map, ini_pop)
        self.sim_island.create_island_map()
        self.years_simulated = 0
        self.final_year = None
        self.vis_years = vis_years
        if img_years is None:
            self.img_years = self.vis_years
        else:
            self.img_years = img_years
        self.img_base = img_base
        self.img_dir = img_dir
        self.img_number = 0
        self.img_fmt = img_fmt

        self.fig = None
        self.ymax_animals = ymax_animals
        if cmax_animals is None:
            self.cmax_animals = {
                "Herbivore": 200,
                "Carnivore": 50
            }
        else:
            self.cmax_animals = cmax_animals
        self.line_graph = None
        self.herb_line = None
        self.carn_line = None
        self.max_ylim = 0

        self.herb_dist = None
        self.herb_dist_data = None
        self.carn_dist = None
        self.carn_dist_data = None

        self.map_visual = None

        self.hist_age = None
        self.hist_weight = None
        self.hist_fitness = None
        if hist_specs is None:
            self.hist_specs = {
                "weight": {"max": 80, "delta": 2},
                "fitness": {"max": 1.0, "delta": 0.05},
                "age": {"max": 80, "delta": 2},
            }
        else:
            self.hist_specs = hist_specs

    @staticmethod
    def set_animal_parameters(species, params):
        """
        Set parameters for animal species.

        Parameters
        ----------
        species: str
            String, name of animal species
        params: dict
            Dict with valid parameter specification for species
        """
        if species == "Herbivore":
            Herbivore.set_parameters(params)
        else:
            Carnivore.set_parameters(params)

    @staticmethod
    def set_landscape_parameters(landscape, params):
        """
        Set parameters for landscape type.

        Parameters
        ----------
        landscape: str
            String, code letter for landscape
        params: dict
            Dict with valid parameter specification for landscape
        """
        if landscape == "W":
            Water.set_parameters(params)
        elif landscape == "H":
            Highland.set_parameters(params)
        elif landscape == "L":
            Lowland.set_parameters(params)
        elif landscape == "D":
            Desert.set_parameters(params)

    def add_population(self, population):
        """
        Add a population to the island.

        Parameters
        ----------
        population: list
            List of dictionaries specifying population
        """
        self.sim_island.add_population(population)

    def simulate(self, num_years):
        """
        Run simulation while visualizing the result.

        Parameters
        ----------
        num_years: int
            Number of years to simulate
        """
        self.final_year = self.years_simulated + num_years

        if self.vis_years != 0:
            self.create_visual()

        while self.years_simulated < self.final_year:

            # writes animal count to logfile
            if self.log_file:
                self.write_log_file()

            # runs the update function every vis_years
            if self.vis_years != 0 and self.years_simulated % self.vis_years == 0:
                self.update_visuals()

            # saves an image every img_years
            if self.img_years != 0 and self.years_simulated % self.img_years == 0:
                self.save_image()

            # runs the annual cycle on the island
            self.sim_island.annual_cycle()
            self.years_simulated += 1

    def write_log_file(self):
        f = open(self.log_file, "a")
        f.write("Year: {years_sim}, "
                "animal count: {num} "
                "where herbivores: {herb_num} and carnivore: {carn_num}\n".format(
                    years_sim=self.years_simulated,
                    num=self.num_animals,
                    herb_num=self.num_animals_per_species["Herbivore"],
                    carn_num=self.num_animals_per_species["Carnivore"]))
        f.close()

    def create_visual(self):
        """
        Creates the plot for all subplot visuals to go on.
        """
        if self.fig is None:
            self.fig = plt.figure(facecolor="#ffffed", figsize=(12, 12))
            self.fig.subplots_adjust(hspace=0.3, wspace=0.2)
            plt.suptitle(f"YEAR: {self.years_simulated}")

        # creates the line graph subplot
        self.create_line_graph()
        # creates the map visualization subplot
        self.create_map_visual()
        # creates the animal distribution subplot
        self.create_anim_dist()
        # creates the histogram subplots
        self.create_hist()

    def create_line_graph(self):
        """
        Creates a plot for visualization of animals.
        """
        # creates a new figure for the visualization
        if self.line_graph is None:
            # positions the line graph subplot
            self.line_graph = self.fig.add_subplot(3, 2, 2)
            # sets ymax if given when initialized
            self.line_graph.set_ylim(0, self.ymax_animals)
            # adds title to subplot
            self.line_graph.title.set_text("ANIMAL COUNT")
            self.line_graph.title.set_weight("bold")
            # labels the x-axis
            self.line_graph.set_xlabel("Year")
            # labels the y-axis
            self.line_graph.set_ylabel("Amount of animals")

        # sets the x-axis as the amount of years
        self.line_graph.set_xlim(0, self.final_year)

        # line for herbivores
        if self.herb_line is None:
            plot_herb = self.line_graph.plot(
                np.arange(0, self.final_year),
                np.full(self.final_year, np.nan),
                "g-",
                label="Herbivore"
            )
            self.herb_line = plot_herb[0]
        else:
            xdata, ydata = self.herb_line.get_data()
            xnew = np.arange(xdata[-1] + 1, self.final_year)
            if len(xnew) > 0:
                ynew = np.full(xnew.shape, np.nan)
                self.herb_line.set_data(
                    np.hstack((xdata, xnew)), np.hstack((ydata, ynew))
                )

        # line for carnivores
        if self.carn_line is None:
            plot_carn = self.line_graph.plot(
                np.arange(0, self.final_year),
                np.full(self.final_year, np.nan),
                "r-",
                label="Carnivore"
            )
            self.carn_line = plot_carn[0]
            # adds a legend
            plt.legend(title="Animals")
        else:
            xdata, ydata = self.carn_line.get_data()
            xnew = np.arange(xdata[-1] + 1, self.final_year)
            if len(xnew) > 0:
                ynew = np.full(xnew.shape, np.nan)
                self.carn_line.set_data(
                    np.hstack((xdata, xnew)), np.hstack((ydata, ynew))
                )

    def create_map_visual(self):
        """
        Creates a visual of the island map to be displayed in thr plot.
        """
        if self.map_visual is None:
            self.map_visual = self.fig.add_subplot(3, 3, 1)
            self.map_visual.title.set_text("Map of the island")
            self.map_visual.title.set_weight("bold")
            self.map_visual.axis("off")

            # defines the rgb values of the cells
            rgb = {
                "W": (0.06, 0.39, 0.49),
                "L": (0.06, 0.49, 0.36),
                "H": (0.60, 0.80, 0.40),
                "D": (1.00, 1.00, 0.40)
            }

            geogr = [[rgb[col] for col in row] for row in self.sim_island.geogr.splitlines()]
            self.map_visual.imshow(geogr)

            map_legend = self.fig.add_axes([0.37, 0.67, 0.1, 0.3])
            map_legend.axis("off")

            for elem_num, name in enumerate(("Water", "Lowland", "Highland", "Desert")):
                map_legend.add_patch(plt.Rectangle(
                    (0., elem_num * 0.2),
                    0.3,
                    0.1,
                    edgecolor="none",
                    facecolor=rgb[name[0]])
                )
                map_legend.text(0.35, elem_num * 0.21, name, transform=map_legend.transAxes)

    def create_anim_dist(self):
        """
        Creates two subplots for the animal distribution
        """
        if self.herb_dist is None:
            self.herb_dist = self.fig.add_subplot(3, 2, 3)
            self.herb_dist.title.set_text("Distribution of herbivores")
            self.herb_dist.title.set_weight("bold")

        if self.carn_dist is None:
            self.carn_dist = self.fig.add_subplot(3, 2, 4)
            self.carn_dist.title.set_text("Distribution of carnivores")
            self.carn_dist.title.set_weight("bold")

    def create_hist(self):
        """
        Creates the histogram subplots.
        """
        if self.hist_age is None:
            self.hist_age = self.fig.add_subplot(6, 3, 16)

        if self.hist_weight is None:
            self.hist_weight = self.fig.add_subplot(6, 3, 17)

        if self.hist_fitness is None:
            self.hist_fitness = self.fig.add_subplot(6, 3, 18)

    def update_visuals(self):
        """
        Updates the plot and it's subplots
        """
        # updates the year counter
        plt.suptitle(f"YEAR: {self.years_simulated}")
        # updates the line graph
        self.update_line_graph()
        # updates the animal distribution
        self.update_dist()
        # updates the histograms
        self.update_hist()
        # pause necessary for the plot to update
        self.fig.canvas.flush_events()
        plt.pause(1e-6)

    def update_line_graph(self):
        """
        Updates the line graph.
        """
        # updates herbivore line
        ydata_herb = self.herb_line.get_ydata()
        ydata_herb[int(self.years_simulated/self.vis_years)] = \
            self.num_animals_per_species["Herbivore"]

        self.herb_line.set_ydata(ydata_herb)

        # updates carnivore line
        ydata_carn = self.carn_line.get_ydata()
        ydata_carn[int(self.years_simulated/self.vis_years)] = \
            self.num_animals_per_species["Carnivore"]

        self.carn_line.set_ydata(ydata_carn)

        # updates the y-axis
        # if a ymax was given, it doesn't change
        if self.ymax_animals:
            pass
        # if no ymax was given, it automatically updates
        else:
            current_y = self.num_animals + 100

            if current_y > self.max_ylim:
                self.max_ylim = current_y

            self.line_graph.set_ylim(0, max(self.max_ylim, current_y))

    def update_dist(self):
        """
        Updates the animal distribution plots.
        """
        if self.herb_dist_data is None:
            self.herb_dist_data = self.herb_dist.imshow(
                self.create_herb_array(),
                interpolation="nearest",
                cmap="summer",
                vmin=0,
                vmax=self.cmax_animals["Herbivore"]
            )
            plt.colorbar(
                self.herb_dist_data, ax=self.herb_dist, orientation="vertical"
            )
        else:
            self.herb_dist_data.set_data(self.create_herb_array())

        if self.carn_dist_data is None:
            self.carn_dist_data = self.carn_dist.imshow(
                self.create_carn_array(),
                interpolation="nearest",
                cmap="summer",
                vmin=0,
                vmax=self.cmax_animals["Carnivore"]
            )
            plt.colorbar(
                self.carn_dist_data, ax=self.carn_dist, orientation="vertical"
            )
        else:
            self.carn_dist_data.set_data(self.create_carn_array())

    def create_herb_array(self):
        """
        Creates an array of herbivores on the island.

        Returns
        -------
        herb_array: array
            array of herbivores on the island
        """
        data = list(self.sim_island.island_map.keys())
        herb_array = np.zeros(data[-1])

        for loc, cell in self.sim_island.island_map.items():
            herb_array[loc[0]-1][loc[1]-1] = len(cell.pop_herbivore)
        return herb_array

    def create_carn_array(self):
        """
        Creates an array of carnivores on the island.

        Returns
        -------
        carn_array: array
            array of carnivores on the island
        """
        data = list(self.sim_island.island_map.keys())
        carn_array = np.zeros(data[-1])

        for loc, cell in self.sim_island.island_map.items():
            carn_array[loc[0]-1][loc[1]-1] = len(cell.pop_carnivore)

        return carn_array

    def update_hist(self):
        """
        Updates the histogram plots.
        """
        self.update_hist_age()
        self.update_hist_weight()
        self.update_hist_fitness()

    def update_hist_age(self):
        """
        Updates the age histogram.
        """
        # clears the previous plot
        self.hist_age.cla()
        self.hist_age.title.set_text("Age distribution")
        self.hist_age.title.set_weight("bold")
        delta_age = self.hist_specs["age"]["delta"]

        herb_data = self.get_age_data_herb()
        if herb_data:
            min_age_herb = min(herb_data)
            max_age_herb = max(herb_data)
            self.hist_age.set_xlim([0, self.hist_specs["age"]["max"]])
            # plots the histograms
            self.hist_age.hist(
                herb_data,
                bins=np.arange(min_age_herb, max_age_herb + delta_age, delta_age),
                histtype="step",
                color="g"
            )

        carn_data = self.get_age_data_carn()
        if carn_data:
            min_age_carn = min(carn_data)
            max_age_carn = max(carn_data)
            self.hist_age.hist(
                carn_data,
                bins=np.arange(min_age_carn, max_age_carn + delta_age, delta_age),
                histtype="step",
                color="r"
            )

    def get_age_data_herb(self):
        """
        Finds the animal's age.

        Returns
        -------
        herb_age: list
            List of age data
        """
        herb_age = []

        for cell in self.sim_island.island_map.values():
            for herb in cell.pop_herbivore:
                herb_age.append(herb.age)
        return herb_age

    def get_age_data_carn(self):
        """
        Finds the animal's age.

        Returns
        -------
        carn_age: list
            List of age data
        """
        carn_age = []

        for cell in self.sim_island.island_map.values():
            for carn in cell.pop_carnivore:
                carn_age.append(carn.age)
        return carn_age

    def update_hist_weight(self):
        """
        Updates the age histogram.
        """
        # clears the previous plot
        self.hist_weight.cla()
        self.hist_weight.title.set_text("Weight distribution")
        self.hist_weight.title.set_weight("bold")
        delta_weight = self.hist_specs["weight"]["delta"]

        herb_data = self.get_weight_data_herb()
        if herb_data:
            min_weight_herb = int(min(herb_data))
            max_weight_herb = int(max(herb_data))
            self.hist_weight.set_xlim([0, self.hist_specs["weight"]["max"]])
            # plots the histograms
            self.hist_weight.hist(
                herb_data,
                bins=np.arange(min_weight_herb, max_weight_herb + delta_weight, delta_weight),
                histtype="step",
                color="g"
            )

        carn_data = self.get_weight_data_carn()
        if carn_data:
            min_weight_carn = int(min(carn_data))
            max_weight_carn = int(max(carn_data))
            self.hist_weight.hist(
                carn_data,
                bins=np.arange(min_weight_carn, max_weight_carn + delta_weight, delta_weight),
                histtype="step",
                color="r"
            )

    def get_weight_data_herb(self):
        """
        Finds the animal's weight.

        Returns
        -------
        herb_weight: list
            List of weight data
        """
        herb_weight = []

        for cell in self.sim_island.island_map.values():
            for herb in cell.pop_herbivore:
                herb_weight.append(herb.weight)
        return herb_weight

    def get_weight_data_carn(self):
        """
        Finds the animal's weight.

        Returns
        -------
        carn_weight: list
            List of weight data
        """
        carn_weight = []

        for cell in self.sim_island.island_map.values():
            for carn in cell.pop_carnivore:
                carn_weight.append(carn.weight)
        return carn_weight

    def update_hist_fitness(self):
        """
        Updates the fitness histogram.
        """
        # clears the previous plot
        self.hist_fitness.cla()
        self.hist_fitness.title.set_text("Fitness distribution")
        self.hist_fitness.title.set_weight("bold")
        delta_fitness = self.hist_specs["fitness"]["delta"]
        min_fitness = 0
        max_fitness = 1

        herb_data = self.get_fitness_data_herb()
        if herb_data:
            self.hist_fitness.set_xlim([0, self.hist_specs["fitness"]["max"]])
            # plots the histograms
            self.hist_fitness.hist(
                herb_data,
                bins=np.arange(min_fitness, max_fitness + delta_fitness, delta_fitness),
                histtype="step",
                color="g"
            )

        carn_data = self.get_fitness_data_carn()
        if carn_data:
            self.hist_fitness.hist(
                carn_data,
                bins=np.arange(min_fitness, max_fitness + delta_fitness, delta_fitness),
                histtype="step",
                color="r"
            )

    def get_fitness_data_herb(self):
        """
        Finds the animal's fitness.

        Returns
        -------
        herb_fitness: list
            List of fitness data
        """
        herb_fitness = []

        for cell in self.sim_island.island_map.values():
            for herb in cell.pop_herbivore:
                herb_fitness.append(herb.fitness)
        return herb_fitness

    def get_fitness_data_carn(self):
        """
        Finds the animal's fitness.

        Returns
        -------
        carn_fitness: list
            List of fitness data
        """
        carn_fitness = []

        for cell in self.sim_island.island_map.values():
            for carn in cell.pop_carnivore:
                carn_fitness.append(carn.fitness)
        return carn_fitness

    def save_image(self):
        """
        Saves an image of the graphics.
        File name is by default {os.path.join(img_dir, img_base}_{img_number:05d}.{img_fmt}
        """
        if self.img_base is None or self.img_dir is None:
            pass
        elif type(self.img_base and self.img_dir) is str:
            plt.savefig(os.path.join(self.img_dir, "{base}_{num:05d}.{type}").format(
                                                                base=self.img_base,
                                                                num=self.img_number,
                                                                type=self.img_fmt))
            self.img_number += 1
        else:
            raise ValueError("img_base and img_dir must both be strings or None")

    @property
    def year(self):
        """
        Last year simulated.

        Returns
        -------
        int
            Last year simulated
        """
        return self.years_simulated

    @property
    def num_animals(self):
        """
        Total number of animals on island.

        Returns
        -------
        num: int
            Number of animals on the island
        """
        num = 0
        for cell in self.sim_island.island_map.values():
            num += len(cell.pop_herbivore)
            num += len(cell.pop_carnivore)
        return num

    @property
    def num_animals_per_species(self):
        """
        Number of animals per species in island, as dictionary.

        Returns
        -------
        num_per_species: dict
            Dictionary of the number of animals per species
        """
        num_per_species = {
            "Herbivore": 0,
            "Carnivore": 0
        }
        for cell in self.sim_island.island_map.values():
            num_per_species["Herbivore"] += len(cell.pop_herbivore)
            num_per_species["Carnivore"] += len(cell.pop_carnivore)
        return num_per_species

    def make_movie(self, movie_fmt="mp4"):
        """Create MPEG4 movie from visualization images saved."""
        os.chdir(self.img_dir)
        if movie_fmt == 'mp4':
            try:
                # Parameters chosen according to http://trac.ffmpeg.org/wiki/Encode/H.264,
                # section "Compatibility"
                subprocess.check_call([_FFMPEG_BINARY,
                                       '-i', '{}_%05d.png'.format(self.img_base),
                                       '-y',
                                       '-profile:v', 'baseline',
                                       '-level', '3.0',
                                       '-pix_fmt', 'yuv420p',
                                       '{}.mp4'.format(self.img_base)])
            except subprocess.CalledProcessError as err:
                raise RuntimeError('ERROR: ffmpeg failed with: {}'.format(err))
        elif movie_fmt == 'gif':
            try:
                subprocess.check_call([_MAGICK_BINARY,
                                       '-delay', '1',
                                       '-loop', '0',
                                       '{}_*.png'.format(self.img_base),
                                       '{}.gif'.format(self.img_base)])
            except subprocess.CalledProcessError as err:
                raise RuntimeError('ERROR: convert failed with: {}'.format(err))
        else:
            raise ValueError('Unknown movie format: ' + movie_fmt +
                             " Valid formats are mp4 and gif")
