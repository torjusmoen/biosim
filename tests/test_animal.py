#!python
# -*- coding: utf-8 -*-

__author__ = "Bastian Undheim Øian, Torjus Strandenes Moen"
__email__ = "bastian.undheim.oian@nmbu.no, torjus.strandenes.moen@nmbu.no"

import pytest
import scipy.stats
from biosim.animals import Herbivore, Carnivore


class TestAnimals:

    @pytest.fixture(autouse=True)
    def create_herbivore(self):
        """
        Creates herbivores to use in tests.
        One with predetermined stats.
        One with random stats.
        """
        determined_age = 5
        determined_weight = 5
        self.herbivore = Herbivore(age=determined_age, weight=determined_weight)

        self.herbivore_list = [self.herbivore for _ in range(5)]

        self.random_herbivore = Herbivore()

        self.determined_age = determined_age
        self.determined_weight = determined_weight

    @pytest.fixture(autouse=True)
    def create_carnivore(self):
        """
        Creates carnivore to use in tests.
        One with predetermined stats.
        One with random stats.
        """
        determined_age = 5
        determined_weight = 5
        self.carnivore = Carnivore(age=determined_age, weight=determined_weight)

        self.random_carnivore = Carnivore()

        self.determined_age = determined_age
        self.determined_weight = determined_weight

    def test_init_herbivore(self):
        """
        Checks if the herbivore initializes correctly with predetermined and random stats
        """
        assert self.herbivore.age == self.determined_age
        assert self.herbivore.weight == self.determined_weight

        assert self.random_herbivore.age == 0
        assert self.random_herbivore.weight > 0
        assert 0 <= self.random_herbivore.fitness <= 1

    def test_init_carnivore(self):
        """
        Checks if the carnivore initializes correctly with predetermined and random stats
        """
        assert self.carnivore.age == self.determined_age
        assert self.carnivore.weight == self.determined_weight

        assert self.random_carnivore.age == 0
        assert self.random_carnivore.weight > 0
        assert 0 <= self.random_carnivore.fitness <= 1

    def test_aging(self):
        """
        Checks if the animal ages correctly
        """
        num_days = 8
        given_age = 4
        self.herbivore.age = given_age
        for _ in range(num_days):
            self.herbivore.aging()
        assert self.herbivore.age == given_age + num_days

    def test_aging_weight(self):
        """
        checks if the weight decreases correctly when the animal ages
        """
        num_days = 4
        given_weight = 5
        expected_weight = 4.07253125
        self.herbivore.weight = given_weight
        for _ in range(num_days):
            self.herbivore.aging()
        assert pytest.approx(self.herbivore.weight) == expected_weight

    def test_value_error(self):
        """
        Checks if unwanted values raises a ValueError
        """
        with pytest.raises(ValueError):
            error_weight = -5
            self.error_herbivore = Herbivore(weight=error_weight)

    def test_death_min_weight(self):
        """
        Checks if calculate_death correctly assesses death from weight
        """
        self.herbivore.weight = 0
        assert self.herbivore.calculate_death()

    def test_death_fitness_max(self):
        """
        Checks if calculate_death correctly assesses survival from max fitness
        """
        self.herbivore.weight = 10
        self.herbivore.fitness = 1
        assert not self.herbivore.calculate_death()

    def test_death_fitness(self, mocker):
        """
        Checks if calculate_death correctly assesses death from fitness with the given equation
        """
        mocker.patch('random.random', return_value=0.1)
        self.herbivore.fitness = 0.5
        assert self.herbivore.calculate_death()

    def test_calculate_fitness_min_weight(self):
        """
        Checks if calculate_fitness correctly calculates the fitness
        """
        self.herbivore.weight = 0
        assert self.herbivore.calculate_fitness() == 0

    def test_calculate_fitness_high_weight(self):
        """
        Checks if calculate_fitness correctly calculates the fitness
        """
        self.herbivore.weight = 20
        expected_fitness = 0.7310585780756752
        assert self.herbivore.calculate_fitness() == expected_fitness

    def test_random_weight(self):
        """
        Checks if the calculate_weight gets a value from a gaussian distribution
        """
        alpha = 0.01
        x = self.herbivore.calculate_weight()
        my = self.herbivore.parameters["w_birth"]
        sigma = self.herbivore.parameters["sigma_birth"]
        z = (x - my) / sigma
        p = scipy.stats.norm(0, 1).cdf(z)
        assert p > alpha

    def test_calculate_birth_weight_min(self):
        """
        Tests if birth is denied if the weight is lower than needed
        """
        self.herbivore.weight = 20
        n = 20
        assert not self.herbivore.calculate_birth(n)

    def test_calculate_birth(self, mocker):
        """
        Tests if calculate_birth correctly assesses birth from adequate fitness
        """
        # This creates 0.2 * 0.6 *(7-1) which is > than 0.5
        mocker.patch('random.random', return_value=0.5)
        self.herbivore.weight = 50
        self.herbivore.fitness = 0.6
        n = 7
        assert self.herbivore.calculate_birth(n)

    def test_chance_denied_calculate_birth(self, mocker):
        """
        Testing denial of birth by chance
        """
        # EQ: 0.2 * 0.5 * (5-1) < 0.5
        mocker.patch('random.random', return_value=0.5)
        self.herbivore.weight = 50
        self.herbivore.fitness = 0.5
        n = 5
        assert not self.herbivore.calculate_birth(n)

    def test_give_birth(self, mocker):
        """
        Testing the function give_birth that the correct weight is given to baby
        """
        mocker.patch('random.random', return_value=0.5)
        mocker.patch('random.gauss', return_value=8)

        self.herbivore.weight = 50
        self.herbivore.fitness = 0.6
        n = 7
        baby_weight = self.herbivore.give_birth(n)
        expected_weight = 8
        assert baby_weight == expected_weight

    def test_fodder_weight_increase(self):
        """
        Tests if the weight increases properly from consume_fodder
        """
        self.herbivore.weight = 30
        expected_weight = 37.2
        self.herbivore.consume_fodder(8)
        assert self.herbivore.weight == expected_weight

    def test_max_fodder(self):
        """
        Tests if the fodder found maximizes at 10
        """
        self.herbivore.weight = 30
        expected_weight = 39
        self.herbivore.consume_fodder(14)
        assert self.herbivore.weight == expected_weight

    def test_fodder_fit_increase(self):
        """
        Test if the fitness changes as expected after consuming food
        """
        self.herbivore.age = 5
        self.herbivore.weight = 40
        self.herbivore.consume_fodder(10)
        expected_fitness = 0.980159693
        assert self.herbivore.fitness == pytest.approx(expected_fitness)

    def test_calculate_migration(self, mocker):
        """
        Checks if the calculate migration gives the right value
        """
        mocker.patch('random.random', return_value=0.25)
        self.herbivore.fitness, self.carnivore.fitness = 0.75, 0.75
        assert not self.herbivore.calculate_migration()  # 0.25 > 0.25 * 0.75
        assert self.carnivore.calculate_migration()  # 0.25 < 0.4 * 0.75

    def test_check_fitness_ratio(self):
        """
        Tests if the check_fitness_ratio can tell when carns fitness is over 0,
        and higher than the herbs fitness,
        and not over DeltaPhiMax
        """
        self.carnivore.fitness = 0.7
        self.herbivore.fitness = 0.5
        assert self.carnivore.check_fitness_ratio(self.herbivore)

    def test_calculate_kill_prob(self):
        """
        Tests if function calculate_kill_prob calculates the true kill probability
        """
        self.carnivore.fitness = 0.8
        self.herbivore.fitness = 0.4
        kill_probability = self.carnivore.calculate_kill_prob(self.herbivore)
        # (0.8 - 0.4)/10 == 0.04
        expected_prob = 0.04
        assert kill_probability == expected_prob

    def test_eat_all_herbivore(self, mocker):
        """
        Tests if all herbivores are killed when correct values are given
        """
        mocker.patch('random.random', return_value=0)
        eaten_herbivores = self.carnivore.eat_herbivores(self.herbivore_list)
        expected_eaten = 5
        assert len(eaten_herbivores) == expected_eaten

    def test_eat_no_herbivore(self, mocker):
        """
        Tests if no herbivores are killed when correct values are given
        """
        mocker.patch('random.random', return_value=1)
        eaten_herbivores = self.carnivore.eat_herbivores(self.herbivore_list)
        expected_eaten = 0
        assert len(eaten_herbivores) == expected_eaten

    def test_weight_eaten(self, mocker):
        """
        Tests if the if sentence for weight eaten will stop the carnivore from killing when
        weight eaten >=  self.parameters["F"]
        """
        mocker.patch('random.random', return_value=0)
        for herbivore in self.herbivore_list:
            herbivore.weight = 49

        eaten_herbivores = self.carnivore.eat_herbivores(self.herbivore_list)
        expected_eaten = 2
        assert len(eaten_herbivores) == expected_eaten

    def test_fitness_no_eat(self, mocker):
        """
        Tests the if sentence that prohibits a carnivore from killing a herbivore when its fitness
        is lower than the herbivores fitness
        """
        mocker.patch('random.random', return_value=0.01)
        self.carnivore.fitness = 0.7
        self.herbivore_list.extend(self.herbivore_list)
        for herbivore in self.herbivore_list:
            herbivore.fitness = 0.8

        eaten_herbivores = self.carnivore.eat_herbivores(self.herbivore_list)
        expected_eaten = 0
        assert len(eaten_herbivores) == expected_eaten

    def test_eat_herb_from_prob(self, mocker):
        """
        Tests if the herbivores are eaten when the kill probability is higher than the random value
        """
        self.carnivore.fitness = 0.4
        mocker.patch('random.random', return_value=0.019)
        for herbivore in self.herbivore_list:
            herbivore.fitness = 0.2
        # Creates probability (0.4 - 0.2)/10 = 0.20
        eaten_herbivores = self.carnivore.eat_herbivores(self.herbivore_list)
        expected_eaten = 5
        assert len(eaten_herbivores) == expected_eaten

    def test_no_eat_herb_from_prob(self, mocker):
        """
        Tests if the herbivores are not eaten
        when the kill probability is lower than the random value
        """
        self.carnivore.fitness = 0.4
        mocker.patch('random.random', return_value=0.021)
        for herbivore in self.herbivore_list:
            herbivore.fitness = 0.2
        # Creates probability (0.4 - 0.2)/10 = 0.20
        eaten_herbivores = self.carnivore.eat_herbivores(self.herbivore_list)
        expected_eaten = 0
        assert len(eaten_herbivores) == expected_eaten
