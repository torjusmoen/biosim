#!python
# -*- coding: utf-8 -*-

__author__ = "Bastian Undheim Øian, Torjus Strandenes Moen"
__email__ = "bastian.undheim.oian@nmbu.no, torjus.strandenes.moen@nmbu.no"

import pytest
from biosim.cell import Cell, Highland, Lowland, Desert, Water
from biosim.animals import Herbivore, Carnivore, Animals


class TestCells:

    @pytest.fixture(autouse=True)
    def animal_list(self):
        """
        Creates a animal_list to use in tests
        """
        animal_list = [{'species': 'Herbivore', 'age': 5, 'weight': 20},
                       {'species': 'Herbivore', 'age': 5, 'weight': 20},
                       {'species': 'Carnivore', 'age': 5, 'weight': 20},
                       {'species': 'Carnivore', 'age': 5, 'weight': 20},
                       {'species': 'Carnivore', 'age': 5, 'weight': 20}]

        null_animal_list = []

        self.cell = Cell(animal_list)

        self.animal_list = animal_list

        for animal in animal_list:
            animal['weight'] = 50

        self.inc_weight_cell = Cell(animal_list)  # Creates a cell of animals with increased weight

        self.lowland1 = Lowland(animal_list)  # Initiates some cells to create migration tests
        self.highland1 = Highland(animal_list)
        self.desert1 = Desert(animal_list)

        self.water0 = Water(null_animal_list)
        self.lowland0 = Lowland(null_animal_list)

        self.lowland1.adjacent_cells = [self.highland1, self.desert1]
        self.lowland0.adjacent_cells = [self.highland1, self.desert1]
        self.highland1.adjacent_cells = [self.lowland0]
        self.desert1.adjacent_cells = [self.lowland0]

    def test_init(self):
        """
        Tests the initiator if it manages to set fMax
        """
        assert self.cell.available_fodder == self.cell.parameters["f_max"]

    def test_pop_herbivore(self):
        """
        Tests if pop_herb is equal to the number of herbivores in animal_list.
        This test therefore also tests if the if sentence in the __init__ works
        """
        amt_herbs = 2
        assert len(self.cell.pop_herbivore) == amt_herbs

    def test_pop_carnivore(self):
        """
        Checks pop_carnivores are equal to the number of carnivores in animal_list
        """
        amt_cars = 3
        assert len(self.cell.pop_carnivore) == amt_cars

    def test_amount_herbivore(self):
        """
        Checks if amount_herbivore returns correct values
        """
        run_amt_herbs = 2
        assert self.cell.amount_herbivore() == run_amt_herbs

    def test_amount_carnivore(self):
        """
        Checks if amount_carnivore returns correct values
        """
        run_amt_car = 3
        assert self.cell.amount_carnivore() == run_amt_car

    def test_add_baby_herbivore(self, mocker):
        """
        Tests if the function add_baby works for herbivores. With 2 herbivores it should make
        2 additional herbivores for the cell
        """
        mocker.patch('random.random', return_value=0)
        self.inc_weight_cell.add_baby()

        expected_herb_amount = 4
        assert len(self.inc_weight_cell.pop_herbivore) == expected_herb_amount

    def test_add_no_baby_herbivore(self, mocker):
        """
        Tests if the function add_baby works when there should be no babies
        """
        mocker.patch('random.random', return_value=1)
        self.inc_weight_cell.add_baby()

        expected_herb_amount = 2
        assert len(self.inc_weight_cell.pop_herbivore) == expected_herb_amount

    def test_add_baby_carnivore(self, mocker):
        """
        Tests if the function add_baby works for carnivore. With 3 carnivores it should make
        3 additional carnivores for the cell
        """
        mocker.patch('random.random', return_value=0)
        self.inc_weight_cell.add_baby()

        expected_carn_amount = 6
        assert len(self.inc_weight_cell.pop_carnivore) == expected_carn_amount

    def test_regenerate_fodder(self):
        """
        Tests if the regenerate_fodder method works correctly.
        """
        self.cell.eating()
        self.cell.regenerate_fodder()
        assert self.cell.available_fodder == self.cell.parameters["f_max"]

    def test_eating_food_surplus(self):
        """
        Testing the function eating when there is a food surplus, should be leftover food
        """

        self.cell.available_fodder = 24
        self.cell.eating()
        assert self.cell.available_fodder > 0

    def test_eating_food_deficit(self):
        """
        Testing function eating in a food deficit. No food remaining
        """

        self.cell.available_fodder = 9
        self.cell.eating()
        no_fodder = 0
        assert self.cell.available_fodder == no_fodder

    def test_aging_herbivore(self, mocker):
        """
        Tests if the cells function aging is called the correct amount of times for herbivores
        """
        mocker.spy(Herbivore, 'aging')  # Spies on the Herbivores aging function
        self.cell.aging()

        assert Herbivore.aging.call_count == len(self.cell.pop_herbivore)

    def test_aging_carnivore(self, mocker):
        """
        Tests if the cells function aging is called the correct amount of times for carnivores
        """
        mocker.spy(Carnivore, 'aging')  # Spies on the Carnivore aging function
        self.cell.aging()

        assert Carnivore.aging.call_count == len(self.cell.pop_carnivore)

    def test_aging_all(self, mocker):
        """
        Tests if the cells function aging is called the correct amount of times for all
        """
        mocker.spy(Animals, 'aging')  # Spies on the Herbivores aging function
        self.cell.aging()

        assert Animals.aging.call_count == len(self.animal_list)

    def test_weight_loss(self):
        """
        Checks if animals lose weight when aging.
        """
        cell = Cell(self.animal_list)
        cell.aging()
        starting_weight = self.animal_list[0]["weight"]
        end_weight = cell.pop_herbivore[0].weight
        assert starting_weight > end_weight

    def test_death_living_herbivore(self, mocker):
        """
        Tests death function for herbivores when they live
        """

        mocker.patch('random.random', return_value=1)  # With value = 1, calculate_death = False
        self.cell.death()
        living_herbs = 2
        assert len(self.cell.pop_herbivore) == living_herbs

    def test_death_herbivore(self, mocker):
        """
        Tests death function for herbivores when they die
        """
        mocker.patch('random.random', return_value=0)  # Value = 0 yields calculate_death = True
        self.cell.death()
        living_herbs = 0
        assert len(self.cell.pop_herbivore) == living_herbs

    def test_death_carnivore(self, mocker):
        """
        Tests death function for carnivores when they live
        """
        mocker.patch('random.random', return_value=1)
        self.cell.death()
        living_cars = 3
        assert len(self.cell.pop_carnivore) == living_cars

    def test_emigration(self, mocker):
        """
        Test that all animals are emigrated out of the cell when all want to move
        """
        mocker.patch('random.random', return_value=0)  # Guarantee animals want to migrate
        self.lowland1.migrate()
        pop = len(self.lowland1.pop_herbivore) + len(self.lowland1.pop_carnivore)
        expected_pop = 0
        assert pop == expected_pop

    def test_immigration(self, mocker):
        """
        Test that the immigration to the cell works
        """
        mocker.patch('random.random', return_value=0)
        self.desert1.migrate()
        self.highland1.migrate()  # desert1 and highland1 only have lowland0 as adjacent cells
        expected_pop_herb = 4
        expected_pop_carn = 6
        assert len(self.lowland0.pop_herbivore) == expected_pop_herb
        assert len(self.lowland0.pop_carnivore) == expected_pop_carn

    def test_has_migrated(self, mocker):
        """
        Tests that no animals can migrate more than once (per cycle as island will reset this)
        """
        mocker.patch('random.random', return_value=0)
        self.highland1.migrate()
        self.desert1.migrate()
        # Runs migrate function on lowland0 but the animals there have already moved
        self.lowland0.migrate()
        expected_pop_herb = 4
        expected_pop_carn = 6
        assert len(self.lowland0.pop_herbivore) == expected_pop_herb
        assert len(self.lowland0.pop_carnivore) == expected_pop_carn

    def test_wont_migrate(self, mocker):
        """
        Tests that the animals wont migrate when they don't want to
        """
        mocker.patch('random.random', return_value=1)
        self.lowland1.migrate()
        expected_pop_herb = 2
        expected_pop_carn = 3
        assert len(self.lowland1.pop_herbivore) == expected_pop_herb
        assert len(self.lowland1.pop_carnivore) == expected_pop_carn

    def test_migration_if_water(self, mocker):
        """
        Tests if the animals wont migrate when there are only adjacent water cells
        (Tests the statement that reports if the access to a cell is either True or False)
        """
        mocker.patch('random.random', return_value=0)
        self.lowland1.adjacent_cells = [self.water0]
        self.lowland1.migrate()
        expected_pop_herb = 2
        expected_pop_carn = 3
        assert len(self.lowland1.pop_herbivore) == expected_pop_herb
        assert len(self.lowland1.pop_carnivore) == expected_pop_carn

    def test_init_highland(self):
        """
        Checks if the highland cell is initialized properly.
        """
        highland = Highland(self.animal_list)
        assert isinstance(highland, Highland)

    def test_init_lowland(self):
        """
        Checks if the water cell is initialized properly.
        """
        lowland = Lowland(self.animal_list)
        assert isinstance(lowland, Lowland)

    def test_init_desert(self):
        """
        Checks if the water cell is initialized properly.
        """
        desert = Desert(self.animal_list)
        assert isinstance(desert, Desert)

    def test_init_water(self):
        """
        Checks if the water cell is initialized properly.
        """
        water = Water(self.animal_list)
        assert isinstance(water, Water)

    def test_highland_params(self):
        """
        Tests if the correct amount of fodder is available on highland and access
        """
        highland = Highland(self.animal_list)
        assert highland.parameters["f_max"] == 300.0
        assert highland.access

    def test_lowland_params(self):
        """
        Tests if the correct amount of fodder is available on lowland and access
        """
        lowland = Lowland(self.animal_list)
        assert lowland.parameters["f_max"] == 800.0
        assert lowland.access

    def test_desert_params(self):
        """
        Tests if the correct amount of fodder is available on desert and access
        """
        desert = Desert(self.animal_list)
        assert desert.parameters["f_max"] == Cell.parameters["f_max"]
        assert desert.access

    def test_water_params(self):
        """
        Tests if the correct amount of fodder is available on water and access
        """
        water = Water(self.animal_list)
        assert water.parameters["f_max"] == Cell.parameters["f_max"]
        assert not water.access
