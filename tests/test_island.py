#!python
# -*- coding: utf-8 -*-

__author__ = "Bastian Undheim Øian, Torjus Strandenes Moen"
__email__ = "bastian.undheim.oian@nmbu.no, torjus.strandenes.moen@nmbu.no"


import collections
import pytest
import textwrap
from biosim.island import Island


class TestIsland:

    @pytest.fixture
    def geogr(self):
        geogr = """\
                   WWW
                   WLW
                   WWW"""
        geogr = textwrap.dedent(geogr)
        return geogr

    @pytest.fixture
    def ini_pop(self):
        return [{'loc': (2, 2),
                 'pop': [{'species': 'Herbivore',
                          'age': 5,
                          'weight': 20} for _ in range(2)]}]

    @pytest.fixture(autouse=True)
    def create_sample_island(self):
        geogr = """\
                           WWW
                           WLW
                           WWW"""
        geogr = textwrap.dedent(geogr)

        ini_pop = [{'loc': (2, 2),
                    'pop': [{'species': 'Herbivore',
                             'age': 5,
                             'weight': 20} for _ in range(2)]}]

        self.animal_list = [{'species': 'Herbivore', 'age': 5, 'weight': 20} for _ in range(2)]

        self.island_map = Island(geogr, ini_pop)

        # Next is used for testing on mating on island cell

        ini_pop_weight = [{'loc': (2, 2),
                           'pop': [{'species': 'Herbivore',
                                    'age': 5,
                                    'weight': 40} for _ in range(2)]}]

        self.inc_weight_map = Island(geogr, ini_pop_weight)

        # Next is used for testing on migration

        migrate_island = """\
                                   WWWWW
                                   WWLWW
                                   WLLLW
                                   WWLWW
                                   WWWWW"""
        migrate_island = textwrap.dedent(migrate_island)

        ini_pop = [{'loc': (3, 3),
                    'pop': [{'species': 'Herbivore',
                             'age': 5,
                             'weight': 20}
                            for _ in range(2)]}]

        self.migrate_island = Island(migrate_island, ini_pop)

    def test_initialization(self):
        """
        Checks if Island can initialize
        """
        assert isinstance(self.island_map, Island)

    def test_cell_dict(self):
        """
        Checks if geogr is correctly made into a dictionary
        """
        self.island_map.create_cell_dict()
        expected_dict = {
            (1, 1): "W", (1, 2): "W", (1, 3): "W",
            (2, 1): "W", (2, 2): "L", (2, 3): "W",
            (3, 1): "W", (3, 2): "W", (3, 3): "W"
        }
        assert self.island_map.cell_dict == expected_dict

    def test_pop_dict(self):
        """
        Checks if ini_pop is correctly made into a dictionary
        """
        self.island_map.create_pop_dict()
        expected_dict = {
            (2, 2): [
                {"species": "Herbivore", "age": 5, "weight": 20},
                {"species": "Herbivore", "age": 5, "weight": 20}
            ]
        }
        assert self.island_map.pop_dict == expected_dict

    def test_add_population(self):
        """
        Tests that the function add_population can
        add animals to their respective lists in the cell on the island
        """
        animal_list = [{'loc': (2, 2),
                        'pop': [{'species': 'Herbivore', 'age': 5, 'weight': 20},
                                {'species': 'Herbivore', 'age': 5, 'weight': 20},
                                {'species': 'Carnivore', 'age': 5, 'weight': 20}]}]

        self.island_map.create_island_map()
        self.island_map.add_population(animal_list)

        assert len(self.island_map.island_map[(2, 2)].pop_herbivore) == 4
        assert len(self.island_map.island_map[(2, 2)].pop_carnivore) == 1

    def test_add_pop_value_error(self):
        """
        Tests that the ValueError is raised when animals are placed in a water cell on Island
        """
        animal_list = [{'loc': (1, 2),
                        'pop': [{'species': 'Herbivore', 'age': 5, 'weight': 20},
                                {'species': 'Herbivore', 'age': 5, 'weight': 20},
                                {'species': 'Carnivore', 'age': 5, 'weight': 20}]}]

        self.island_map.create_island_map()

        with pytest.raises(ValueError):
            assert self.island_map.add_population(animal_list)

    def test_annual_cycle(self, mocker):
        """
        Tests if annual cycle runs the given functions
        """
        mocker.spy(Island, 'regen_fodder')
        mocker.spy(Island, 'eating')
        mocker.spy(Island, 'migration')
        mocker.spy(Island, 'add_baby')
        mocker.spy(Island, 'aging')
        mocker.spy(Island, 'death')
        self.island_map.annual_cycle()

        assert Island.regen_fodder.call_count == 1
        assert Island.eating.call_count == 1
        assert Island.migration.call_count == 1
        assert Island.add_baby.call_count == 1
        assert Island.aging.call_count == 1
        assert Island.death.call_count == 1

    def test_eating_weight(self):
        """
        Checks if eating correctly feeds the animals
        """
        self.island_map.create_island_map()
        starting_weight = self.island_map.island_map[(2, 2)].pop_herbivore[0].weight
        self.island_map.regen_fodder()
        self.island_map.eating()
        end_weight = self.island_map.island_map[(2, 2)].pop_herbivore[0].weight
        assert starting_weight < end_weight

    def test_eating_fitness(self):
        """
        Checks if the animals fitness is correctly updated after eating
        """
        self.island_map.create_island_map()
        starting_fitness = self.island_map.island_map[(2, 2)].pop_herbivore[0].fitness
        self.island_map.regen_fodder()
        self.island_map.eating()
        end_fitness = self.island_map.island_map[(2, 2)].pop_herbivore[0].fitness
        assert starting_fitness < end_fitness

    def test_regen_fodder(self, ini_pop):
        """
        Checks if regen_fodder resets the fodder amount
        """
        geogr_fodder = """\
                        WWWW
                        WHLW
                        WHDW
                        WWWW"""
        geogr_fodder = textwrap.dedent(geogr_fodder)
        island_map = Island(geogr_fodder, ini_pop)
        island_map.create_island_map()
        fodder_before = 0
        for x in range(1, 4):
            for y in range(1, 4):
                fodder_before = fodder_before + island_map.island_map[(x, y)].available_fodder
        island_map.regen_fodder()
        fodder_after = 0
        for x in range(1, 4):
            for y in range(1, 4):
                fodder_after = fodder_after + island_map.island_map[(x, y)].available_fodder

        assert fodder_before < fodder_after

    def test_find_adjacent_cells(self):
        """
        Tests if the function find_adjacent cells can find the right adjacent cells
        """
        self.island_map.create_island_map()  # find_adjacent_cells is build in to create_map_island
        current_cell = self.island_map.island_map[(2, 2)]
        expected_list = [self.island_map.island_map[(1, 2)],
                         self.island_map.island_map[(2, 1)],
                         self.island_map.island_map[(2, 3)],
                         self.island_map.island_map[(3, 2)]]

        adjacent_cells = collections.Counter(current_cell.adjacent_cells)
        expected_adjacent_cells = collections.Counter(expected_list)
        assert adjacent_cells == expected_adjacent_cells

    def test_migration(self, mocker):
        """
        Tests if animals on island migrates when they should
        """
        mocker.patch("random.random", return_value=0)
        self.migrate_island.create_island_map()
        starting_animals = len(self.migrate_island.island_map[(3, 3)].pop_herbivore)
        self.migrate_island.migration()
        end_animals = len(self.migrate_island.island_map[(3, 3)].pop_herbivore)
        assert starting_animals > end_animals

    def test_give_birth(self, mocker):
        """
        Checks if animals give birth when they should
        """
        mocker.patch("random.random", return_value=0)
        self.inc_weight_map.create_island_map()
        starting_amt = len(self.inc_weight_map.island_map[(2, 2)].pop_herbivore)
        self.inc_weight_map.add_baby()
        end_amt = len(self.inc_weight_map.island_map[(2, 2)].pop_herbivore)
        assert starting_amt < end_amt

    def test_aging(self):
        """
        Checks if aging correctly ages the animal
        """
        self.island_map.create_island_map()
        starting_age = self.island_map.island_map[(2, 2)].pop_herbivore[0].age
        self.island_map.aging()
        end_age = self.island_map.island_map[(2, 2)].pop_herbivore[0].age
        assert starting_age < end_age

    def test_death(self, mocker):
        """
        Checks if animals die as they should
        """
        mocker.patch("random.random", return_value=0)
        self.island_map.create_island_map()
        starting_animals = len(self.island_map.island_map[(2, 2)].pop_herbivore)
        self.island_map.death()
        end_animals = len(self.island_map.island_map[(2, 2)].pop_herbivore)
        assert starting_animals > end_animals
